//
//  RewardModel.swift
//  adidas
//
//  Created by Michael on 26/10/2019.
//  Copyright © 2019 Michael. All rights reserved.
//

import Foundation

/// UI model for reward
struct RewardModel {

    var points:Int64 = 0
    var trophy:String = ""

}
