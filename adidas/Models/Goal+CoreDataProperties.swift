//
//  Goal+CoreDataProperties.swift
//  adidas
//
//  Created by Michael on 25/10/2019.
//  Copyright © 2019 Michael. All rights reserved.
//
//

import Foundation
import CoreData


extension Goal {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Goal> {
        return NSFetchRequest<Goal>(entityName: "Goal")
    }

    @NSManaged public var id: String?
    @NSManaged public var title: String?
    @NSManaged public var goal_description: String?
    @NSManaged public var type: String?
    @NSManaged public var goal: Int64
    @NSManaged public var reward: Reward?

}
