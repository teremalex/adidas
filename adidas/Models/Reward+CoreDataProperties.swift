//
//  Reward+CoreDataProperties.swift
//  adidas
//
//  Created by Michael on 25/10/2019.
//  Copyright © 2019 Michael. All rights reserved.
//
//

import Foundation
import CoreData


extension Reward {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Reward> {
        return NSFetchRequest<Reward>(entityName: "Reward")
    }

    @NSManaged public var trophy: String?
    @NSManaged public var points: Int64

}
