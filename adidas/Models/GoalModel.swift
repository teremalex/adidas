//
//  GoalModel.swift
//  adidas
//
//  Created by Michael on 26/10/2019.
//  Copyright © 2019 Michael. All rights reserved.
//

import Foundation

/// UI model for goal
struct GoalModel {
    
    var id:String = ""
    var title:String = ""
    var type:String = ""
    var goal_description:String = ""
    var goal:Int64 = 0
    
    var reward:RewardModel = RewardModel()
    
}
