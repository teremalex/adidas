//
//  GoalsService.swift
//  adidas
//
//  Created by Michael on 25/10/2019.
//  Copyright © 2019 Michael. All rights reserved.
//

import UIKit
import Alamofire
import CoreData
import HealthKit

let GoalsListURL = "https://thebigachallenge.appspot.com/_ah/api/myApi/v1/goals"

/// Main purpose of this service is to manipulate with Goals and Rewards in terms of network, CoreData and ViewModels
protocol GoalServiceProtocol {
    
    func calculateScores() -> (Int64)

    /// Downloads json data from link GoalsListURL and saves in CoreData. In case of error or success calls completion block
    ///
    /// - Parameter completion: indicates that network request finished
    func downloadGoals(completion: @escaping () -> Void)

    /// Reads all goals from CoreaData and generates models for view
    ///
    /// - Returns: list of goals models for UI
    func fetchAllGoals() -> ([GoalModel])
    
    /// Provides number of steps from HealthKit for today
    ///
    /// - Parameter completion: number of steps
    func todaysSteps(completion: @escaping (Double) -> Void)

    /// Provides distance to run
    ///
    /// - Parameter completion: distance range
    func todaysDistance(completion: @escaping (Double) -> Void)

}

class GoalService: GoalServiceProtocol {
    
    public var healthStore = HKHealthStore()

    //  MARK: - GoalServiceProtocol -

    func calculateScores() -> (Int64) {
        let goalsList = fetchAllGoals()
        var distance:Int64 = 0
        var steps:Int64 = 0
        let dispatchGroup = DispatchGroup()
        dispatchGroup.enter()
        todaysDistance { (distanceForToday) in
            distance = Int64(distanceForToday)
            dispatchGroup.leave()
        }
        dispatchGroup.wait()
        dispatchGroup.enter()
        todaysSteps { (stepsForToday) in
            steps = Int64(stepsForToday)
            dispatchGroup.leave()
        }
        dispatchGroup.wait()
        
        var scores:Int64 = 0
        
        for goal in goalsList {
            switch goal.type {
            case "step":
                if steps >= goal.goal {
                    scores = scores + goal.reward.points
                }
            case "running_distance", "walking_distance":
                if distance >= goal.goal {
                    scores = scores + goal.reward.points
                }
            default:
                break
            }
        }
        
        return scores
    }
    
    func todaysDistance(completion: @escaping (Double) -> Void) {
        todaysValue(valueId: .distanceWalkingRunning, completion: completion)
    }
    
    func todaysSteps(completion: @escaping (Double) -> Void) {
        todaysValue(valueId: .stepCount, completion: completion)
    }
    
    func downloadGoals(completion: @escaping () -> Void) {
        Alamofire.request(GoalsListURL,
                          method: .get,
                          parameters: nil).validate().responseJSON { response in
                            guard response.result.isSuccess else {
                                print("HTTP error")
                                completion()
                                return
                            }
                            
                            guard let jsonGoals = response.result.value as? [String: Any] else {
                                print("Incorrect data received from backend")
                                completion()
                                return
                            }
                            
                            self.saveJson2CoreData(jsonGoals: jsonGoals["items"] as! [[String : Any]])
                            completion()
        }
    }
    
    func fetchAllGoals() -> ([GoalModel]) {
        var goalsModelsList = [GoalModel]()

        guard let coredataGoalsList:[Goal] = Goal.mr_findAll() as? [Goal] else {
            return goalsModelsList
        }
        
        if (coredataGoalsList.count == 0) {
            return goalsModelsList
        }
        
        for goal in coredataGoalsList {
            var newModel = GoalModel()
            newModel.goal = goal.goal
            newModel.goal_description = goal.goal_description ?? ""
            newModel.id = goal.id ?? ""
            newModel.title = goal.title ?? ""
            newModel.type = goal.type ?? ""
            
            var newReward = RewardModel()
            newReward.trophy = goal.reward?.trophy ?? ""
            newReward.points = goal.reward?.points ?? 0
            newModel.reward = newReward
            
            goalsModelsList.append(newModel)
        }
        
        return goalsModelsList
    }
    
    //  MARK: - Routine -

    private func saveJson2CoreData(jsonGoals:[[String: Any]]) {
        if jsonGoals.count == 0 {
            return
        }
        
        for jsonGoal in jsonGoals {
            NSManagedObjectContext.mr_default().mr_save(blockAndWait:{(localContext) in
                let goalId = jsonGoal["id"] as? String
                let checkPredicate = NSPredicate(format: "id = %@", goalId!)
                let existingGoal = Goal.mr_findFirst(with: checkPredicate)
                if existingGoal != nil {
                    return
                }

                guard let newGoal = Goal.mr_createEntity(in: localContext) else {
                    return
                }
                
                newGoal.id = goalId
                newGoal.goal = jsonGoal["goal"] as! Int64
                newGoal.goal_description = jsonGoal["description"] as? String
                newGoal.title = jsonGoal["title"] as? String
                newGoal.type = jsonGoal["type"] as? String
                
                guard let newReward = Reward.mr_createEntity(in: localContext) else {
                    return
                }
                let jsonReward = jsonGoal["reward"] as! [String:Any]
                newReward.trophy = jsonReward["trophy"] as? String
                newReward.points = jsonReward["points"] as! Int64
                newGoal.reward = newReward
            })
        }
    }

    private func todaysValue(valueId:HKQuantityTypeIdentifier, completion: @escaping (Double) -> Void) {
        let curentDateTime = Date()
        let stepsPredicate = HKQuery.predicateForSamples(withStart: Calendar.current.startOfDay(for: curentDateTime),
                                                         end: curentDateTime,
                                                         options: .strictStartDate)
        
        let query = HKStatisticsQuery(quantityType: HKQuantityType.quantityType(forIdentifier: valueId)!,
                                      quantitySamplePredicate: stepsPredicate,
                                      options: .cumulativeSum) { _, result, _ in
                                        guard let result = result, let stepsQuantity = result.sumQuantity() else {
                                            completion(0)
                                            return
                                        }
                                        
                                        completion(stepsQuantity.doubleValue(for: HKUnit.count()))
        }
        
        healthStore.execute(query)
    }
    
}
