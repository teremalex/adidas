//
//  GoalsListScoreCell.swift
//  adidas
//
//  Created by Michael on 27/10/2019.
//  Copyright © 2019 Michael. All rights reserved.
//

import UIKit

/// Cell to show users score
class GoalsListScoreCell: UITableViewCell {

    public func defineScores() {
        let goalsService:GoalServiceProtocol = GoalService()
        textLabel?.text = "Scores: \(goalsService.calculateScores())"
        detailTextLabel?.text = "Number of points you've already earned"
    }
    
}
