//
//  ViewController.swift
//  adidas
//
//  Created by Michael on 25/10/2019.
//  Copyright © 2019 Michael. All rights reserved.
//

import UIKit

let GoalsListCellID = "GoalsListCellID"
let GoalsListScoreCellID = "GoalsListScoreCellID"

/// Main screen with list of goals
class GoalsListVC: UITableViewController {
    
    private var goalsList = [GoalModel]()

    override func viewDidLoad() {
        super.viewDidLoad()

        let goalService:GoalServiceProtocol = GoalService()
        goalService.downloadGoals {
            self.goalsList = goalService.fetchAllGoals()
            self.tableView.reloadData()
        }
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rowsNumber = 0
        
        switch section {
        case 0:
            rowsNumber = 1
        case 1:
            rowsNumber = goalsList.count
        default:
            break
        }
        
        return rowsNumber
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var newCell = UITableViewCell.init()

        switch indexPath.section {
        case 0:
            let scoreCell:GoalsListScoreCell = tableView.dequeueReusableCell(withIdentifier: GoalsListScoreCellID) as! GoalsListScoreCell
            scoreCell.defineScores()
            newCell = scoreCell
        case 1:
            let goalsCell:GoalsListCell = tableView.dequeueReusableCell(withIdentifier: GoalsListCellID) as! GoalsListCell
            goalsCell.defineModel(model: goalsList[indexPath.row])
            newCell = goalsCell

        default:
            break
        }
        
        return newCell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SegueOpenGoalDetailsVC" {
            if let detailsViewController = segue.destination as? GoalDetailsVC {
                let listCell:GoalsListCell = sender as! GoalsListCell
                detailsViewController.goal = listCell.currentModel()
            }
        }
    }
    
}
