//
//  GoalsListCell.swift
//  adidas
//
//  Created by Michael on 26/10/2019.
//  Copyright © 2019 Michael. All rights reserved.
//

import UIKit

/// Cell for goals in main list of main screen
class GoalsListCell: UITableViewCell {

    private var goal:GoalModel = GoalModel()

    public func defineModel(model:GoalModel) {
        goal = model

        textLabel?.text = goal.title
        detailTextLabel?.text = goal.goal_description
        
        var icon = UIImage.init(named:goal.reward.trophy)
        if icon == nil {
            icon = UIImage.init(named:"unknown_reward")
        }
        imageView?.image = icon
    }
    
    public func currentModel() -> (GoalModel) {
        return goal
    }

}
