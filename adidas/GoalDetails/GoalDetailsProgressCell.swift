//
//  GoalDetailsProgressCell.swift
//  adidas
//
//  Created by Michael on 27/10/2019.
//  Copyright © 2019 Michael. All rights reserved.
//

import UIKit

let BattleMessages = ["All you need is just to start!",
                      "Way to go!",
                      "It's half the way",
                      "You're almost DONE it!",
                      "YEAH!! THE REWARD IS YOURS!"]

class GoalDetailsProgressCell:UITableViewCell {
    
    @IBOutlet private weak var statusConstraint: NSLayoutConstraint!
    @IBOutlet private weak var battleMessage: UILabel!

    public func defineReward(goal:GoalModel) {
        let goalsService:GoalServiceProtocol = GoalService()
        switch goal.type {
        case "step":
            goalsService.todaysSteps { (stepsNumber) in
                self.displayResult(percentage:CGFloat(stepsNumber) / CGFloat(goal.goal))
            }
        case "running_distance", "walking_distance":
            goalsService.todaysDistance { (distance) in
                self.displayResult(percentage:CGFloat(distance) / CGFloat(goal.goal))
            }
        default:
            break
        }
    }
    
    //  MARK: - Routine -

    private func displayResult(percentage:CGFloat) {
        var borderChecked = percentage
        if borderChecked > 1 {
            borderChecked = 1
        }
        
        DispatchQueue.main.async {
            self.defineBattleMessage(percentage: borderChecked)
            self.statusConstraint.constant = self.frame.width * (1 - CGFloat(borderChecked))
        }
    }
    
    private func defineBattleMessage(percentage:CGFloat) {
        if percentage >= 1 {
            self.battleMessage.text = BattleMessages[4]
        }
        
        if (percentage >= 0) && (percentage < 0.3) {
            self.battleMessage.text = BattleMessages[0]
        }
        
        if (percentage >= 0.3) && (percentage < 0.5) {
            self.battleMessage.text = BattleMessages[1]
        }
        
        if (percentage >= 0.5) && (percentage < 0.8) {
            self.battleMessage.text = BattleMessages[2]
        }
        
        if (percentage >= 0.8) && (percentage < 1) {
            self.battleMessage.text = BattleMessages[3]
        }
    }
    
}
