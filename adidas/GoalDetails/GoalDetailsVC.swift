//
//  GoalDetails.swift
//  adidas
//
//  Created by Michael on 26/10/2019.
//  Copyright © 2019 Michael. All rights reserved.
//

import UIKit

class GoalDetailsVC: UITableViewController {
    
    public var goal:GoalModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rowsNumber = 0
        
        switch section {
        case 0: //  main info
            rowsNumber = 4
        case 1: //  reward info
            rowsNumber = 1
        case 2: //  progress
            rowsNumber = 1
        default:
            rowsNumber = 0
        }
        
        return rowsNumber
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var newcell = UITableViewCell.init()
        
        let cellService:GoalDetailsCellServiceProtocol = GoalDetailsCellService()
        
        switch indexPath.section {
        case 0: //  main info
            if goal != nil {
                newcell = cellService.createInfoCell(tableView: tableView, cellIndex: indexPath.row, goal: goal!)
            }
        case 1: //  reward info
            if goal != nil {
                newcell = cellService.createRewardCell(tableView: tableView, reward: goal!.reward)
            }
        case 2: //  progress info
            if goal != nil {
                newcell = cellService.createProgressCell(tableView: tableView, goal: goal!)
            }
        default:
            newcell = UITableViewCell.init()
        }
        
        return newcell
    }

}
