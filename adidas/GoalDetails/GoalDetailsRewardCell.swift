//
//  GoalDetailsRewardCell.swift
//  adidas
//
//  Created by Michael on 26/10/2019.
//  Copyright © 2019 Michael. All rights reserved.
//

import UIKit

class GoalDetailsRewardCell:UITableViewCell {
    
    @IBOutlet private weak var rewardIcon: UIImageView!
    @IBOutlet private weak var scores: UILabel!

    public func defineReward(reward:RewardModel) {
        rewardIcon.image = UIImage.init(named: reward.trophy)
        scores.text = "REWARD \(reward.points) POINTS"
    }
    
}
