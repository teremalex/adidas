//
//  GoalDetailsCell.swift
//  adidas
//
//  Created by Michael on 26/10/2019.
//  Copyright © 2019 Michael. All rights reserved.
//

import UIKit

class GoalDetailsInfoCell: UITableViewCell {

    public func defineFields(fieldTitle:String, fieldValue:String) {
        textLabel?.text = fieldTitle
        detailTextLabel?.text = fieldValue
    }
    
}
