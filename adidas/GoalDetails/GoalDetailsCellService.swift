//
//  GoalDetailsCellService.swift
//  adidas
//
//  Created by Michael on 26/10/2019.
//  Copyright © 2019 Michael. All rights reserved.
//

import UIKit

let GoalsDetailsInfoCellID = "GoalsDetailsInfoCellID"
let GoalsDetailsRewardCellID = "GoalsDetailsRewardCellID"
let GoalsDetailsProgressCellID = "GoalsDetailsProgressCellID"

protocol GoalDetailsCellServiceProtocol {
    
    func createInfoCell(tableView:UITableView, cellIndex:Int, goal:GoalModel) -> (UITableViewCell)
    func createRewardCell(tableView:UITableView, reward:RewardModel) -> (UITableViewCell)
    func createProgressCell(tableView:UITableView, goal:GoalModel) -> (UITableViewCell)
    
}

class GoalDetailsCellService: GoalDetailsCellServiceProtocol {
    
    //  MARK: - GoalDetailsCellServiceProtocol -

    func createInfoCell(tableView:UITableView, cellIndex:Int, goal:GoalModel) -> (UITableViewCell) {
        let newCell:GoalDetailsInfoCell = tableView.dequeueReusableCell(withIdentifier: GoalsDetailsInfoCellID) as! GoalDetailsInfoCell

        switch cellIndex {
        case 0:
            newCell.defineFields(fieldTitle: "Title: ", fieldValue: goal.title)
        case 1:
            newCell.defineFields(fieldTitle: "Type: ", fieldValue: goal.type)
        case 2:
            newCell.defineFields(fieldTitle: "Value: ", fieldValue: "\(goal.goal)")
        case 3:
            newCell.defineFields(fieldTitle: "Description: ", fieldValue: goal.goal_description)
        case 4:
            newCell.defineFields(fieldTitle: "Reward: ", fieldValue: goal.goal_description)
        default:
            break
        }

        return newCell
    }
    
    func createRewardCell(tableView:UITableView, reward:RewardModel) -> (UITableViewCell) {
        let newCell:GoalDetailsRewardCell = tableView.dequeueReusableCell(withIdentifier: GoalsDetailsRewardCellID) as! GoalDetailsRewardCell
        newCell.defineReward(reward: reward)

        return newCell
    }
    
    func createProgressCell(tableView:UITableView, goal:GoalModel) -> (UITableViewCell) {
        let newCell:GoalDetailsProgressCell = tableView.dequeueReusableCell(withIdentifier: GoalsDetailsProgressCellID) as! GoalDetailsProgressCell
        newCell.defineReward(goal: goal)
        
        return newCell
    }
    
}
